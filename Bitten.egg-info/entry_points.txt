[bitten.recipe_commands]
http://bitten.edgewall.org/tools/c#make = bitten.build.ctools:make
http://bitten.cmlenz.net/tools/c#configure = bitten.build.ctools:configure
http://bitten.edgewall.org/tools/php#phing = bitten.build.phptools:phing
http://bitten.edgewall.org/tools/c#cppunit = bitten.build.ctools:cppunit
http://bitten.cmlenz.net/tools/sh#exec = bitten.build.shtools:exec_
http://bitten.edgewall.org/tools/java#ant = bitten.build.javatools:ant
http://bitten.cmlenz.net/tools/svn#checkout = bitten.build.svntools:checkout
http://bitten.cmlenz.net/tools/python#unittest = bitten.build.pythontools:unittest
http://bitten.cmlenz.net/tools/sh#pipe = bitten.build.shtools:pipe
http://bitten.cmlenz.net/tools/python#pylint = bitten.build.pythontools:pylint
http://bitten.edgewall.org/tools/svn#export = bitten.build.svntools:export
http://bitten.edgewall.org/tools/java#cobertura = bitten.build.javatools:cobertura
http://bitten.cmlenz.net/tools/c#cppunit = bitten.build.ctools:cppunit
http://bitten.edgewall.org/tools/c#cunit = bitten.build.ctools:cunit
http://bitten.cmlenz.net/tools/hg#pull = bitten.build.hgtools:pull
http://bitten.cmlenz.net/tools/php#phing = bitten.build.phptools:phing
http://bitten.cmlenz.net/tools/php#coverage = bitten.build.phptools:coverage
http://bitten.cmlenz.net/tools/java#ant = bitten.build.javatools:ant
http://bitten.edgewall.org/tools/mono#nunit = bitten.build.monotools:nunit
http://bitten.cmlenz.net/tools/python#coverage = bitten.build.pythontools:coverage
http://bitten.edgewall.org/tools/python#figleaf = bitten.build.pythontools:figleaf
http://bitten.edgewall.org/tools/xml#transform = bitten.build.xmltools:transform
http://bitten.edgewall.org/tools/php#phpunit = bitten.build.phptools:phpunit
http://bitten.edgewall.org/tools/svn#update = bitten.build.svntools:update
http://bitten.cmlenz.net/tools/c#cunit = bitten.build.ctools:cunit
http://bitten.cmlenz.net/tools/c#make = bitten.build.ctools:make
http://bitten.cmlenz.net/tools/python#figleaf = bitten.build.pythontools:figleaf
http://bitten.edgewall.org/tools/python#coverage = bitten.build.pythontools:coverage
http://bitten.edgewall.org/tools/svn#checkout = bitten.build.svntools:checkout
http://bitten.edgewall.org/tools/c#configure = bitten.build.ctools:configure
http://bitten.edgewall.org/tools/php#coverage = bitten.build.phptools:coverage
http://bitten.cmlenz.net/tools/c#gcov = bitten.build.ctools:gcov
http://bitten.cmlenz.net/tools/c#autoreconf = bitten.build.ctools:autoreconf
http://bitten.cmlenz.net/tools/xml#transform = bitten.build.xmltools:transform
http://bitten.cmlenz.net/tools/python#trace = bitten.build.pythontools:trace
http://bitten.cmlenz.net/tools/java#cobertura = bitten.build.javatools:cobertura
http://bitten.edgewall.org/tools/java#junit = bitten.build.javatools:junit
http://bitten.cmlenz.net/tools/java#junit = bitten.build.javatools:junit
http://bitten.edgewall.org/tools/python#trace = bitten.build.pythontools:trace
http://bitten.edgewall.org/tools/python#exec = bitten.build.pythontools:exec_
http://bitten.cmlenz.net/tools/python#exec = bitten.build.pythontools:exec_
http://bitten.edgewall.org/tools/c#autoreconf = bitten.build.ctools:autoreconf
http://bitten.edgewall.org/tools/sh#pipe = bitten.build.shtools:pipe
http://bitten.cmlenz.net/tools/php#phpunit = bitten.build.phptools:phpunit
http://bitten.edgewall.org/tools/sh#exec = bitten.build.shtools:exec_
http://bitten.cmlenz.net/tools/svn#export = bitten.build.svntools:export
http://bitten.edgewall.org/tools/hg#pull = bitten.build.hgtools:pull
http://bitten.cmlenz.net/tools/python#distutils = bitten.build.pythontools:distutils
http://bitten.cmlenz.net/tools/mono#nunit = bitten.build.monotools:nunit
http://bitten.edgewall.org/tools/c#gcov = bitten.build.ctools:gcov
http://bitten.edgewall.org/tools/python#pylint = bitten.build.pythontools:pylint
http://bitten.edgewall.org/tools/python#unittest = bitten.build.pythontools:unittest
http://bitten.edgewall.org/tools/python#distutils = bitten.build.pythontools:distutils
http://bitten.cmlenz.net/tools/svn#update = bitten.build.svntools:update

[trac.plugins]
bitten.main = bitten.main
bitten.master = bitten.master
bitten.coverage = bitten.report.coverage
bitten.web_ui = bitten.web_ui
bitten.notify = bitten.notify
bitten.admin = bitten.admin
bitten.testing = bitten.report.testing
bitten.lint = bitten.report.lint

[console_scripts]
bitten-slave = bitten.slave:main

[distutils.commands]
unittest = bitten.util.testrunner:unittest

